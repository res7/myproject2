/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.model;

import db.helper.ConfigInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import project.thrift.user.EStatusResult;
import project.thrift.user.TUser;
import project.thrift.user.TUserListResult;
import project.thrift.user.TUserResult;
    
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;

/**
 *
 * @author dat
 */
public class UserDA {

    private static final String SELECT_QUERY = "SELECT * FROM `%s`";

    private static final String CHECK_USER_EXISTS_QUERY = SELECT_QUERY + " WHERE `userName`=? or `numberPhone` = ?";

    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `userName`,`passwordSalt`,`passwordHash`,`displayName`,`email`, `numberPhone`,`refreshToken`, `isVerify`, `isBan`, `balance`) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ?";

    private static final UserDA instance = new UserDA();
    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";

    private static final Logger logger = Logger.getLogger(UserDA.class);
    private static final String UPDATE_INFORMATION_QUERY = "UPDATE `%s` SET `displayName` = ?, `passwordHash` = ?, "
            + "`email` = ?, `numberPhone` = ? WHERE `id` = ?;";
    private static final String UPDATE_BAN = "UPDATE `%s` SET  `isBan` = ? WHERE `id` = ?;";
    private static final String UPDATE_VERIFY    = "UPDATE `%s` SET  `isVerify` = ? WHERE `id` = ?;";
    private static final String GET_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";


    public static UserDA getInstance() {
        return instance;
    }

    public long getLastID() {
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(GET_LASTID, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    private static TUser createFromReader(ResultSet rs) throws SQLException {
        TUser user = new TUser();
        user.setId(rs.getLong("id"));
        user.setUserName(rs.getString("userName"));
        user.setPasswordSalt(rs.getString("passwordSalt"));
        user.setPasswordHash(rs.getString("passwordHash"));
        user.setDisplayName(rs.getString("displayName"));
        user.setEmail(rs.getString("email"));
        user.setNumberPhone(rs.getString("numberPhone"));
        user.setRefreshToken(rs.getString("refreshToken"));
        user.setIsVerify(rs.getBoolean("isVerify"));
        user.setIsBan(rs.getBoolean("isBan"));
        user.setBalance(rs.getDouble("balance"));
        return user;
    }

    public boolean checkUserExists(String userName, String numberPhone) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(CHECK_USER_EXISTS_QUERY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            stmt.setString(2, numberPhone);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean insertUser(TUser value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getUserName());
            stmt.setString(3, value.getPasswordSalt());
            stmt.setString(4, value.getPasswordHash());
            stmt.setString(5, value.getDisplayName());
            stmt.setString(6, value.getEmail());
            stmt.setString(7, value.getNumberPhone());
            stmt.setString(8, value.getRefreshToken());
            stmt.setBoolean(9, value.isVerify);
            stmt.setBoolean(10, value.isBan);
            stmt.setDouble(11, value.getBalance());

            System.out.println(stmt);

            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }

    public boolean delete(TUser value) throws SQLException {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, value.getId());
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean updateInformation(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_INFORMATION_QUERY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, value.getDisplayName());
          
            stmt.setString(2, value.getPasswordHash());

            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public TUserResult getById(long id) {
        TUserResult result = new TUserResult();
        TUser user = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                user = createFromReader(rs);
            }
            result.setValue(user);
            result.setStatus(EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean verify(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_VERIFY, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setBoolean(2, Boolean.TRUE);
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean ban(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_BAN, ConfigInfo.TABLE_USER);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setBoolean(2, Boolean.TRUE);
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserListResult getList(String whereClause) {
        TUserListResult result = new TUserListResult();
        List<TUser> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_USER, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TUser item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
}
