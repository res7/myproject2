/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.model;

import db.helper.ConfigInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import project.thrift.auth.EStatusResult;

import project.thrift.auth.TAuth;
import project.thrift.auth.TAuthResponse;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;

/**
 *
 * @author dat
 */
public class AuthDA {

    private static final AuthDA instance = new AuthDA();
    private static final Logger logger = Logger.getLogger(AuthDA.class);
    private static final String SELECT_QUERY = "SELECT * FROM `%s`";
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ?";
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `userId`, `secretKey`) VALUES (?,?,?);";
    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";

    public static AuthDA getInstance() {
        return instance;
    }

    private static TAuth createFromReader(ResultSet rs) throws SQLException {
        TAuth auth = new TAuth();
        auth.setId(rs.getLong("id"));
        auth.setUserId(rs.getLong("userId"));
        auth.setSecretkey(rs.getString("secretKey"));
        return auth;
    }

    public TAuthResponse getById(long id) {
        TAuthResponse result = new TAuthResponse();
        TAuth auth = new TAuth();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_AUTH);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                auth = createFromReader(rs);
            }
            result.setAuth(auth);
            result.setStatus(EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean isAuthExist(long userId) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_AUTH);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, userId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean insertAuthDA(TAuth value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_AUTH);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getSecretkey());
            stmt.setLong(3, value.getUserId());
            System.out.println(stmt);

            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }

    public boolean delete(TAuth value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_AUTH);
        try ( PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, value.getId());
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean update(TAuth value) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
