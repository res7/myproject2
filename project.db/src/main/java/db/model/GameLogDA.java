/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.model;

import db.helper.ConfigInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import project.thrift.gameLog.EStatusResult;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import project.thrift.gameLog.TGameLogResult;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;

/**
 *
 * @author dat
 */
public class GameLogDA {

    private static final Logger logger = Logger.getLogger(GameLogDA.class);

    private static final GameLogDA instance = new GameLogDA();
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";

    public static GameLogDA getInstance() {
        return instance;
    }
    private static final String SELECT_QUERY = "SELECT * FROM `%s`";
    private static final String GET_BY_USERID_QUERY = SELECT_QUERY + " WHERE `userId`= ? ;";

    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`=?";
    private static final String GET_BY_USER_GAME_DAY_QUERY = SELECT_QUERY + " WHERE `userId`= ? AND `idGame`= ? AND `modifiedAt` LIKE ?";
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `userId`,``,`idGame`,`goldChange`, `createAt`, `modifiedAt`) VALUES (?,?,?,?,NOW(),NOW());";
    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`=?";

    private static TGameLog createFromReader(ResultSet rs) throws SQLException {
        TGameLog item = new TGameLog();
        item.setId(rs.getLong("id"));
        item.setUserId(rs.getLong("userId"));
        item.setGameId(rs.getLong("idGame"));
        item.setGoldChange(rs.getDouble("goldChange"));
        item.setCreateAt(rs.getString("createAt"));
        item.setModifiedAt(rs.getString("modifiedAt"));
        return item;
    }

    public TGameLogResult getById(long id) {
        TGameLogResult result = new TGameLogResult();
        TGameLog value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_GAMELOG);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setGameLog(value);
            result.setStatus(value == null ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public TGameLogListResult getListByUser(long userId) {
        TGameLogListResult result = new TGameLogListResult();
        List<TGameLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USERID_QUERY, ConfigInfo.TABLE_GAMELOG);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, userId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGameLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            System.out.println(stmt);
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0 ? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public TGameLogListResult getListByUserGameDay(long userId, long idGame, String modifiedAt) {
        TGameLogListResult result = new TGameLogListResult();
        List<TGameLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_GAME_DAY_QUERY, ConfigInfo.TABLE_GAMELOG);
        System.out.println(idGame);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, userId);
            stmt.setLong(2, idGame);
            stmt.setString(3, modifiedAt+"%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGameLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }

            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            System.out.println(lstValue);
            //set data
            result.setStatus(totalRecord == 0 ? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public Long getLastID() {
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(GET_LASTID, ConfigInfo.TABLE_GAMELOG);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }

        return result;
    }

    public boolean insertGameLog(TGameLog value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_GAMELOG);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setLong(2, value.getUserId());
            stmt.setLong(3, value.getGameId());
            stmt.setDouble(4, value.getGoldChange());
            stmt.setString(5, value.getCreateAt());
            stmt.setString(6, value.getModifiedAt());
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }

    public boolean delete(TGameLog value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_GAMELOG);
        try ( PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, value.getId());
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

//    public boolean update(TGameLog value) {
//        boolean result = false;
//        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
//        Connection cnn = cm.borrow();
//        String query = String.format(UPDATE_QUERY, ConfigInfo.TABLE_GAME);
//        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
//            stmt.setLong(4, value.getId());
//            stmt.setString(1, value.getGameName());
//            stmt.setString(2, value.getDescription());
//            stmt.setInt(3, value.getStatus());
//            result = stmt.executeUpdate() > 0;
//        } catch (SQLException ex) {
//            logger.error(ex);
//        } finally {
//            cm.giveBack(cnn);
//        }
//        return result;
//    }
}
