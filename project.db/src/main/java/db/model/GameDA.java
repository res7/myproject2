/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.model;

import db.helper.ConfigInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import project.thrift.game.EStatusResult;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.game.TGameResult;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;

/**
 *
 * @author dat
 */
public class GameDA {

    private static final Logger logger = Logger.getLogger(GameDA.class);
    private static final String SELECT_QUERY = "SELECT * FROM `%s`";
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    private static final String CHECK_EXISTS_QUERY = SELECT_QUERY + " WHERE `gameName`=? ;";
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ?";
    private static final String GET_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `gameName`,`description`) VALUES (?,?,?);";
    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";
    private static final String UPDATE_QUERY = "UPDATE `%s` SET `gameName` = ?, `description` = ? WHERE `id` = ?;";
    private static final GameDA instance = new GameDA();

    public static GameDA getInstance() {
        return instance;
    }

    private static TGame createFromReader(ResultSet rs) throws SQLException {
        TGame item = new TGame();
        item.setId(rs.getLong("id"));
        item.setGameName(rs.getString("gameName"));
        item.setDescription(rs.getString("description"));
        return item;
    }

    public long getLastID() {
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(GET_LASTID, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean checkExists(String gameName) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(CHECK_EXISTS_QUERY, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, gameName);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public TGameResult getById(long id) {
        TGameResult result = new TGameResult();
        TGame value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }

    public boolean delete(TGame value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, value.getId());
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    public boolean update(TGame value) {
         boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_QUERY, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(3, value.getId());
            stmt.setString(1, value.getGameName());
            stmt.setString(2, value.getDescription());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    public boolean insertGame(TGame value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_GAME);
        try ( PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getGameName());
            stmt.setString(3, value.getDescription());

//            System.out.println(stmt);
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }

        public TGameListResult getList(String whereClause) {
        TGameListResult result = new TGameListResult();
        List<TGame> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_GAME, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGame item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
}
