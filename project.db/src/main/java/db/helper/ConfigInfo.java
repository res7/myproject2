/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.helper;

import vn.outa.framework.util.ConfigUtil;
import vn.outa.framework.util.ConvertUtil;

/**
 *
 * @author dat
 */
public class ConfigInfo {

    public static final String DB_CONFIG = "db_game";

//    public static String TABLE_LOG = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_game_log"), "tb_game_log");
//    
    public static String TABLE_USER = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_user"), "tb_user");
    public static String TABLE_AUTH = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_auth"), "tb_auth");
    public static String TABLE_GAMELOG = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_gameLog"), "tb_gameLog");
    public static String TABLE_GAME = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_game"), "tb_game");

}

