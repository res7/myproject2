/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package function;

import com.google.gson.reflect.TypeToken;
import db.model.GameDA;
import java.lang.reflect.Type;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJobResult;
import org.gearman.client.GearmanJobResultImpl;
import org.gearman.util.ByteUtils;
import org.gearman.worker.AbstractGearmanFunction;
import project.thrift.game.TGame;
import project.thrift.user.EJobType;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.EncryptUtil;
import vn.outa.framework.util.JsonUtil;

/**
 *
 * @author dat
 */
public class GameFunction extends AbstractGearmanFunction {

    private static final Logger logger = Logger.getLogger(GameFunction.class);

    @Override
    public GearmanJobResult executeFunction() {
        String returnMsg = "";
        boolean result = false;
        try {
            String jobData = EncryptUtil.decodeUTF8((byte[]) this.data);
            Type type = new TypeToken<JobEnt<EJobType>>() {
            }.getType();

            JobEnt<EJobType> job = (JobEnt) JsonUtil.deserialize(jobData, type);
            if (job.data != null) {
                TGame value = new TGame();
                switch (job.type) {
                    case CREATE:
                        value = JsonUtil.deserialize(job.data, TGame.class);
                        result = GameDA.getInstance().insertGame(value);
                        break;
                    case DELETE:
                        value = JsonUtil.deserialize(job.data, TGame.class);
                        result = GameDA.getInstance().delete(value);
                        break;
                    case UPDATE:
                        value = JsonUtil.deserialize(job.data, TGame.class);
                        result = GameDA.getInstance().update(value);
                        break;

                    default:
                        returnMsg = "Invalid job action : " + data;
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            returnMsg = e.getMessage();
        }

        // log error in case of failures
        if (!result) {
            logger.error(returnMsg);
        }
        // return gearman job result
        byte[] returnData = result ? (byte[]) this.data : new byte[0];
        return new GearmanJobResultImpl(this.jobHandle, result, new byte[0], returnData, ByteUtils.toUTF8Bytes(returnMsg), 0, 0);
    }

}
