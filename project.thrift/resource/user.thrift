include "TServiceBase.thrift"

namespace java project.thrift.user

enum EStatusResult {
    OK = 0,
    FAIL = 1
}
enum EJobType {
    CREATE,
    UPDATE,
    BAN,
    VERIFY,
    DELETE,
    UPDATE_MONEY,
    LOGIN_USER,
    CHECK_USER_EXISTS,
    GET_ALL_USER,
    GET_USER_BY_ID
}
struct TUser { 
    1:i64 id,
    2:string userName,
    3:string passwordSalt,
    4:string passwordHash,
    5:string displayName,
    6:string email,
    7:string numberPhone,
    8:string refreshToken,
    9:bool isVerify,
    10:bool isBan,
    11:double balance,
}
struct TUserResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TUser value
}
struct TRequestLogin {
    1:string userName,
    2:string password
}
struct TUserListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TUser> listData,
    3: i64 totalRecord
}

service UserService extends TServiceBase.ServiceBases{
    bool ping(),
    bool create(1:TUser user),
    bool update(1:TUser user),
    bool remove(1:i64 idUser),
    bool ban(1:i64 idUser),
    bool verify(1:i64 idUser),
    bool checkUserExists(1:TUser user),
    TUserResult login(1:TRequestLogin login),
    TUserResult getById(1:i64 id),
    TUserListResult getAllUser()
}