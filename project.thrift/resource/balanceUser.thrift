namespace java project.thrift.balanceUser
include "TServiceBase.thrift"
enum EJobType {
    CREATE,
    UPDATE,
    DELETE,
    UPDATE_TOTALGOLD,
    UPDATE_STATUS,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
    DELETE = 3
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}

struct TGoldStatistic {
    1: i64 id,
    2: i64 idUser,
    3: string gameName,
    4: double totalGold,
    5: string day,
    6: i32 status,
}

struct TGoldStatisticResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGoldStatistic value
}

struct TGoldStatisticListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGoldStatistic> listData,
    3: i64 totalRecord
}

service GoldStatisticService extends TServiceBases.ServiceBases {
    bool create(1: TGoldStatistic value),
    bool update(1: TGoldStatistic value),
    bool remove(1: i64 id),
    bool updateStatus(1:TGoldStatistic value),
    bool updateGold(1:TGoldStatistic value),
    TGoldStatisticResult getById(1: i64 id),
    TGoldStatisticListResult getMulti(1: string whereClause, 2: i32 offset, 3: i32 count)
    TGoldStatisticListResult getListByUser(1:string userName),
}

