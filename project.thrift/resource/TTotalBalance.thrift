include "TServiceBase.thrift"
namespace java project.thrift.totalBalance
struct GameList{
    1:i64 id,
    2:string game,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1,
}
struct TGameLog {
    1: i64 id,
    2: i64 userId,
    3: i64 gameId,
    4: double goldBalance,
    5: double goldChange,
    6: string createdAt,
    7: string modifiedAt,
}
struct TGameLogResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGameLog value
}

struct TGameLogListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGameLog> listData,
    3: i64 totalRecord
}

service gameLog extends TServiceBase.ServiceBases{
    bool create(1:TGameLog value),
    bool update(1:TGameLog value),
    bool remove(1:i64 id),
    TGameLogResult getById(1:i64 id),
    TGameLogListResult getMulti(1:string whereClause, 2:i32 offset, 3:i32 count),
    TGameLogListResult getListGoldByDate(1:GameList listgame,2:string date),
    TGameLogListResult getListHistoryGoldByGame(1:i64 gameId,2:string date),

}