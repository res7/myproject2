include "TServiceBase.thrift"

namespace java project.thrift.game

enum EJobType {
    CREATE,
    UPDATE,
    UPDATE_STATUS,
    DELETE,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
    DELETE = 3
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}

struct TGame {
    1: i64 id,
    2: string gameName,
    3: string description,
}

struct TGameResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGame value
}

struct TGameListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGame> listData,
    3: i64 totalRecord
}

service GameService extends TServiceBase.ServiceBases {
    bool create(1: TGame value),
    bool update(1: TGame value),
    bool remove(1: i64 id),
    bool checkExists(1:string value),
    TGameResult getById(1: i64 id),
    TGameListResult getAllGame(),
}

