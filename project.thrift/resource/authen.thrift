namespace java project.thrift.auth
include "TServiceBase.thrift"

struct TAuth{
    1:i64 id,
    2:i64 userId,
    3:string secretkey,
}
 enum EJobType {
    CREATE,
    UPDATE,
    DELETE,
    
}
enum EStatusResult {
    OK = 0,
    FAIL = 1
}
struct TAuthResponse {
    1:EStatusResult status = EStatusResult.OK,
    2:TAuth auth 
}
service AuthService extends TServiceBase.ServiceBases{
    TAuthResponse getSecretKey(1:i64  UserID), 
    bool create(1:TAuth auth),
    bool remove(1:i64 UserID),
    bool update(1:TAuth auth),
}

