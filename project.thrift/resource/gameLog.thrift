include "TServiceBase.thrift"

namespace java project.thrift.gameLog

enum EJobType {
    CREATE,
    UPDATE,
    DELETE,
    UPDATE_GOLD,
    UPDATE_STATUS,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
    DELETE = 3
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}
struct TGameLog {
    1: i64 id,
    2:i64 userId,
    3:i64 gameId
    4: double goldChange,
    5: string createAt,
    6: string modifiedAt,
}
struct TGameLogResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGameLog gameLog
}

struct TGameLogListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGameLog> listData,
    3: i64 totalRecord
}

service gameLogService extends TServiceBase.ServiceBases {
    bool create(1:TGameLog gameLog),
    bool update(1:TGameLog gameLog),
    bool remove(1:i64 id),
    bool updateStatus(1:TGameLog gameLog),
    TGameLogResult getById(1:i64 id),
    TGameLogListResult getListByID(1:i64 userId, 2:i32 offset, 3:i32 count),
    TGameLogListResult getListByGameDay (1:i64 userId, 2:i64 idGame, 3:string modifiedAt, 4:i32 offset, 5:i32 count)
    TGameLogListResult getListRank(1:i32 offset, 2:i32 count),
}