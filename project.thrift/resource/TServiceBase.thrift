
namespace java project.thrift.TService

enum ServiceStatus {
    STARTED = 0,
    STOPPED = 1,
    SHUTTING_DOWN = 2
}





service ServiceBases {
    bool ping()
}
