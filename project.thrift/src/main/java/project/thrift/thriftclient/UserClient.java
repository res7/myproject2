/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package project.thrift.thriftclient;

import static javax.management.Query.value;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import project.thrift.user.EStatusResult;
import project.thrift.user.TRequestLogin;
import project.thrift.user.TUser;
import project.thrift.user.TUserListResult;
import project.thrift.user.TUserResult;
import project.thrift.user.UserService;
import project.thrift.user.UserService.Client;

import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;

/**
 *
 * @author dat
 */
public class UserClient implements UserService.Iface {

    private static final Logger logger = Logger.getLogger(UserClient.class);
    private final TClientManager clientManager;

    public UserClient(String section) {
        clientManager = TClientManager.getInstance(section, UserService.class);
    }

    @Override
    public boolean ping() throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean create(TUser user) throws TException {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(user);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean update(TUser user) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean remove(long idUser) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean ban(long idUser) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean verify(long idUser) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean checkUserExists(TUser user) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TUserResult login(TRequestLogin login) throws TException {
        TUserResult result;
        TClientObject<Client> tco = clientManager.borrow();

        try {
            result = tco.client.login(login);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("login error", ex);
            clientManager.invalidate(tco);
            result = new TUserResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TUserResult getById(long id) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TUserListResult getAllUser() throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
