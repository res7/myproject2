/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package project.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import project.thrift.game.GameService;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.game.TGameResult;
import project.thrift.game.EStatusResult;
import project.thrift.game.GameService.Client;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;



/**
 *
 * @author dat
 */
public class GameClient implements GameService.Iface {

    private static final Logger logger = Logger.getLogger(GameClient.class);
    private final TClientManager clientManager;

    public GameClient(String section) {
        clientManager = TClientManager.getInstance(section, GameService.class);
    }

    @Override
    public boolean create(TGame value) throws TException {
          boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;    
    }

    @Override
    public boolean update(TGame value) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean remove(long id) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean checkExists(String value) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TGameResult getById(long id) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TGameListResult getAllGame() throws TException {
        TGameListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getAllGame();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get all game error", ex);
            clientManager.invalidate(tco);
            result = new TGameListResult(EStatusResult.FAIL, null,0);
        }
        return result;  
    }

    @Override
    public boolean ping() throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
