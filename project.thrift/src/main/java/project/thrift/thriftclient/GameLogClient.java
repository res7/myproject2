/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package project.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import project.thrift.gameLog.EStatusResult;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import project.thrift.gameLog.TGameLogResult;
import project.thrift.gameLog.gameLogService;
import project.thrift.gameLog.gameLogService.Client;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;

/**
 *
 * @author dat
 */
public class GameLogClient implements gameLogService.Iface {

    private static final Logger logger = Logger.getLogger(GameLogClient.class);
    private final TClientManager clientManager;

    public GameLogClient(String section) {
        clientManager = TClientManager.getInstance(section, gameLogService.class);
    }

    @Override
    public boolean create(TGameLog gameLog) throws TException {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(gameLog);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean update(TGameLog gameLog) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean remove(long id) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean updateStatus(TGameLog gameLog) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TGameLogResult getById(long id) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TGameLogListResult getListByID(long userId, int offset, int count) throws TException {
        TGameLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();

        try {
            result = tco.client.getListByID(userId, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list by game day errrr", ex);
            clientManager.invalidate(tco);
            result = new TGameLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

    @Override
    public TGameLogListResult getListByGameDay(long userId, long idGame, String modifiedAt, int offset, int count) throws TException {
        TGameLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListByGameDay(userId, idGame, modifiedAt, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list by game day errrr", ex);
            clientManager.invalidate(tco);
            result = new TGameLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

    @Override
    public TGameLogListResult getListRank(int offset, int count) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean ping() throws TException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
