/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author dat
 */
public class GameInDay {
    private String gameName;
    private Double totalBalanceInGame;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

   
    public Double getTotalBalanceInGame() {
        return totalBalanceInGame;
    }

    public void setTotalBalanceInGame(Double totalBalanceInGame) {
        this.totalBalanceInGame = totalBalanceInGame;
    }
    
}
