/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author dat
 */
public class RequestGameDay {
    private String Token;
    private String Date;
    public String getToken() {
        return Token;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }
    
}
