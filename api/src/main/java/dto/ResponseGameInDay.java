/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import java.util.List;

/**
 *
 * @author dat
 */
public class ResponseGameInDay {
    List<GameInDay> listGame ;  

    public List<GameInDay> getListGame() {
        return listGame;
    }

    public void setListGame(List<GameInDay> listGame) {
        this.listGame = listGame;
    }
}
