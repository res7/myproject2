/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import Util.UtilJWT;
import dto.ForgotPassword;
import dto.LoginRequest;
import dto.RegisterResponse;
import entity.registerUser;
import static helper.ConfigInfo.THRIFT_SERVICE;
import java.security.SecureRandom;
import org.apache.thrift.TException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import project.thrift.thriftclient.UserClient;
import project.thrift.user.TRequestLogin;
import project.thrift.user.TUser;
import project.thrift.user.TUserResult;

/**
 *
 * @author dat
 */
@Service
public class UserService {

    private static final UserClient clientUser = new UserClient(THRIFT_SERVICE);

    public static RegisterResponse login(TRequestLogin loginx) throws TException {
        TUser user = clientUser.login(loginx).getValue();
        int strength = 10; // work factor of bcrypt
        BCryptPasswordEncoder bCryptPasswordEncoder
                = new BCryptPasswordEncoder(strength, new SecureRandom());
        boolean isPasswordCorrect = bCryptPasswordEncoder.matches(loginx.getPassword(), user.getPasswordHash());
        if (isPasswordCorrect) {
            RegisterResponse res = new RegisterResponse();
            res.setDisplayName(user.getDisplayName());
            res.setEmail(user.getEmail());
            res.setUsername(user.getUserName());
//            String token = jwtUtils.generateToken(user);
            res.setToken(UtilJWT.createJWT("123", user.getUserName(), String.valueOf(user.getId()), 100000000));
            res.setStatus("OK");
            return res;
        }
        RegisterResponse res = new RegisterResponse();
        res.setStatus("Fail");
        return res;
    }

    public static RegisterResponse forgot(ForgotPassword resx) throws TException {
        TRequestLogin login = new TRequestLogin();
        login.setUserName(resx.getUserName());
        login.setPassword(resx.getOldPassword());
        TUser user = clientUser.login(login).getValue();
        int strength = 10; // work factor of bcrypt
        BCryptPasswordEncoder bCryptPasswordEncoder
                = new BCryptPasswordEncoder(strength, new SecureRandom());
        boolean isPasswordCorrect = bCryptPasswordEncoder.matches(resx.getOldPassword(), user.getPasswordHash());
        if (isPasswordCorrect) {

//            BCryptPasswordEncoder bCryptPasswordEncoderx
//                = new BCryptPasswordEncoder(strength, new SecureRandom());
            String encodedPassword = bCryptPasswordEncoder.encode(resx.getNewPassword());
            user.setPasswordHash(encodedPassword);
            boolean isUpdate = clientUser.update(user);
            if (isUpdate) {
                RegisterResponse res = new RegisterResponse();
                res.setDisplayName(user.getDisplayName());
                res.setEmail(user.getEmail());
                res.setUsername(user.getUserName());
                res.setToken(UtilJWT.createJWT("123", user.getUserName(), String.valueOf(user.getId()), 100000000));
                res.setStatus("OK");
                return res;

            }
             RegisterResponse res = new RegisterResponse();
        res.setStatus("Fail");
        return res;
        }
        RegisterResponse res = new RegisterResponse();
        res.setStatus("Fail");
        return res;
    }

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public String encodePass(String password) {
        String encodedPassword = bCryptPasswordEncoder.encode(password);
        return encodedPassword;
    }

    public static RegisterResponse register(registerUser rUser) throws TException {
        TUser user = new TUser();
        user.setUserName(rUser.getUserName());
        int strength = 10; // work factor of bcrypt
        BCryptPasswordEncoder bCryptPasswordEncoder
                = new BCryptPasswordEncoder(strength, new SecureRandom());
        String encodedPassword = bCryptPasswordEncoder.encode(rUser.getPassword());
        user.setPasswordHash(encodedPassword);
        user.setDisplayName(rUser.getDisplayName());
        user.setEmail(rUser.getEmail());
        user.setNumberPhone(rUser.getNumberPhone());
        user.setRefreshToken("");
        user.setIsVerify(true);
        user.setIsBan(false);
        user.setBalance(0);
        boolean result = clientUser.create(user);
        if (result) {
            RegisterResponse res = new RegisterResponse();
            res.setDisplayName(user.getDisplayName());
            res.setEmail(user.getEmail());
            res.setUsername(user.getUserName());
            res.setToken(UtilJWT.createJWT("123", user.getUserName(), user.getEmail(), 10000));
            res.setStatus("OK");
            return res;
        }
        RegisterResponse res = new RegisterResponse();
        res.setStatus("Fail");
        return res;
    }

}
