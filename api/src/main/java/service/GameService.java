/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dto.ResponseDefault;
import static helper.ConfigInfo.THRIFT_SERVICE;
import org.apache.thrift.TException;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.thriftclient.GameClient;

/**
 *
 * @author dat
 */
public class GameService {

    private static final GameClient gameClient = new GameClient(THRIFT_SERVICE);

    public static TGameListResult getListGame() throws TException {
        return gameClient.getAllGame();
    }

    public static ResponseDefault addGame(TGame game) throws TException {
        boolean isAddGameSuccess = gameClient.create(game);
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
