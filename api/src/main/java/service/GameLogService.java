/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import Util.UtilJWT;
import dto.GameInDay;
import dto.RegisterResponse;
import dto.RequestGameDay;
import dto.ResponseGameInDay;
import static helper.ConfigInfo.THRIFT_SERVICE;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.thrift.TException;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import project.thrift.thriftclient.GameClient;
import project.thrift.thriftclient.GameLogClient;

/**
 *
 * @author dat
 */
public class GameLogService {

    private static final GameLogClient clientGameLog = new GameLogClient(THRIFT_SERVICE);
    private static final GameClient clientGame = new GameClient(THRIFT_SERVICE);

    public static TGameLogListResult getGameLog(RequestGameDay rqGameDay, String idGame) throws TException {
        String userID = UtilJWT.decodeJWT(rqGameDay.getToken()).getSubject();
        String date = rqGameDay.getDate();
        return clientGameLog.getListByGameDay(Long.valueOf(userID), Long.valueOf(idGame), date, 0, -1);
    }

    public static ResponseGameInDay getBalanceByGameDay(RequestGameDay rqGameDay) throws TException {
        String userID = UtilJWT.decodeJWT(rqGameDay.getToken()).getSubject();
        String date = rqGameDay.getDate();
        TGameListResult list = clientGame.getAllGame();
        List<GameInDay> value = new ArrayList<>();
            System.out.println(value);
            System.out.println("service "+list.getListData());

        for (TGame game : list.getListData()) {

            value.add(filter(clientGameLog.getListByGameDay(Long.valueOf(userID), game.getId(), date, 0, -1), game.getId(), game.getGameName()));
        }
        ResponseGameInDay result = new ResponseGameInDay();
        result.setListGame(value);
        return result;
    }

    public static GameInDay filter(TGameLogListResult list, long id, String gameName) {
        GameInDay result = new GameInDay();
        result.setGameName(gameName);
        double totalDay = 0;
        if (list.getListData() != null) {
            for (TGameLog gameLog : list.getListData()) {
                totalDay += gameLog.getGoldChange();
            }

        }
        result.setTotalBalanceInGame(totalDay);
        return result;
    }

    public static List<TGameLog> getAllLog(RequestGameDay request) throws TException {
        String userID = UtilJWT.decodeJWT(request.getToken()).getSubject();
        return clientGameLog.getListByID(Long.valueOf(userID), 0, -1).getListData();
    }
}
