/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dto.ResponseDefault;
import org.apache.thrift.TException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import service.GameService;

/**
 *
 * @author dat
 */
@RestController
@RequestMapping("/api/v1/game")
public class GameController {
    @GetMapping
    public TGameListResult getAllGame() throws TException{
        return GameService.getListGame();
    }
    @PostMapping
    public ResponseDefault addGame(@RequestBody TGame game) throws TException{
        return GameService.addGame(game);
    }
}
