/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import Util.UtilJWT;
import dto.ForgotPassword;
import dto.LoginRequest;
import entity.registerUser;
import dto.RegisterResponse;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import project.thrift.user.TRequestLogin;
import service.UserService;

/**
 *
 * @author dat
 */
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @PostMapping(path = "/register")
    public RegisterResponse register(@RequestBody registerUser user) throws TException {

        return UserService.register(user);
    }
    @PostMapping(path="/login")
    public RegisterResponse login(@RequestBody TRequestLogin login) throws TException{
        return UserService.login(login);
    }
    @PostMapping(path="/forgotpassword")
    public RegisterResponse forgot(@RequestBody ForgotPassword res){
        return UserService.forgot(res);
    }
}
