/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dto.RequestGameDay;
import dto.ResponseGameInDay;
import java.util.List;
import org.apache.thrift.TException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import service.GameLogService;

/**
 *
 * @author dat
 */
@RestController
@RequestMapping("/api/v1/log")
public class GameLogController {
    @PostMapping(path = "/game/{idGame}")
    public TGameLogListResult getListMoneyADay(@RequestBody RequestGameDay rqGameDay,@PathVariable("idGame") String idGame) throws TException {

        return GameLogService.getGameLog(rqGameDay,idGame);
    }
     @PostMapping(path = "/allgameinday")
    public ResponseGameInDay getBalanceByGameDay(@RequestBody RequestGameDay rqGameDay) throws TException{
        return GameLogService.getBalanceByGameDay(rqGameDay);
    }
    @PostMapping(path="/log/user")
    public List<TGameLog> getAllLog(@RequestBody RequestGameDay request) throws TException{
        return GameLogService.getAllLog(request);
    }
      @PostMapping(path = "/test")
    public RequestGameDay getBalanceByGameDayx(@RequestBody RequestGameDay rqGameDay) throws TException{
        return rqGameDay;   
    }
}
