/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package thrift;

import business.GameLogBC;
import java.util.List;
import org.apache.thrift.TException;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import project.thrift.gameLog.TGameLogResult;
import project.thrift.gameLog.gameLogService.Iface;

/**
 *
 * @author dat
 */
public class GameLogProcessor implements Iface{

    @Override
    public boolean create(TGameLog gameLog) throws TException {
        return GameLogBC.create(gameLog);
    }

    @Override
    public boolean update(TGameLog gameLog) throws TException {
        return GameLogBC.update(gameLog);
    }

    @Override
    public boolean remove(long id) throws TException {
        return GameLogBC.remove(id);
    }

    @Override
    public boolean updateStatus(TGameLog gameLog) throws TException {
        return GameLogBC.updateStatus(gameLog);
    }

    @Override
    public TGameLogResult getById(long id) throws TException {
        return GameLogBC.getById(id);
    }

    @Override
    public TGameLogListResult getListByID(long userId, int offset, int count) throws TException {
        return GameLogBC.getListByID(userId,offset,count);
    }

    

    @Override
    public TGameLogListResult getListRank(int offset, int count) throws TException {
        return GameLogBC.getListRank(offset,count);
    }

    @Override
    public boolean ping() throws TException {
        return true;
    }

    @Override
    public TGameLogListResult getListByGameDay(long userId, long idGame, String modifiedAt, int offset, int count) throws TException {
        return GameLogBC.getListByGameDay(userId,idGame,modifiedAt,offset,count);
    }
    
}
