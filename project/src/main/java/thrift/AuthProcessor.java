/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package thrift;
import business.AuthBC;

import org.apache.thrift.TException;
import project.thrift.auth.AuthService.Iface;
import project.thrift.auth.TAuth;
import project.thrift.auth.TAuthResponse;

/**
 *
 * @author dat
 */
public class AuthProcessor implements Iface{

    @Override
    public TAuthResponse getSecretKey(long UserID) throws TException {
        return AuthBC.getSecretKey(UserID);
    }

    @Override
    public boolean ping() throws TException {
        return true;
    }

    @Override
    public boolean create(TAuth auth) throws TException {
        return AuthBC.create(auth);
    }

    @Override
    public boolean remove(long UserID) throws TException {
        return AuthBC.remove(UserID);
    }

    @Override
    public boolean update(TAuth auth) throws TException {
        return AuthBC.update(auth);
    }

    private boolean isAuthExist(TAuth auth) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
