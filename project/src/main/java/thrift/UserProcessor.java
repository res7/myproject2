/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package thrift;
import business.UserBC;
import org.apache.thrift.TException;
import project.thrift.user.TRequestLogin;
import project.thrift.user.TUser;
import project.thrift.user.TUserListResult;
import project.thrift.user.TUserResult;
import project.thrift.user.UserService.Iface;
/**
 *
 * @author dat
 */
public class UserProcessor  implements Iface{

    @Override
    public boolean ping() throws TException {
        return true;
    }

    @Override
    public boolean create(TUser user) throws TException {
        return UserBC.create(user);
        }

    @Override
    public boolean update(TUser user) throws TException {
        return UserBC.update(user);
    }

    @Override
    public boolean remove(long idUser) throws TException {
        return UserBC.remove(idUser);
    }

    @Override
    public boolean ban(long idUser) throws TException {
        return UserBC.ban(idUser);
    }

    @Override
    public boolean verify(long idUser) throws TException {
        return UserBC.verify(idUser);
    }

    @Override
    public boolean checkUserExists(TUser user) throws TException {
        return UserBC.checkUserExists(user);
    }

   

    @Override
    public TUserResult getById(long id) throws TException {
        return UserBC.getById(id);
    }

    @Override
    public TUserListResult getAllUser() throws TException {
        return UserBC.getAllUser();
    }

    @Override
    public TUserResult login(TRequestLogin login) throws TException {
        return UserBC.login(login);
    }
    
}
