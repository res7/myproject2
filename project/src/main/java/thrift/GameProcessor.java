/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package thrift;

import business.GameBC;
import org.apache.thrift.TException;
import project.thrift.game.GameService.Iface;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.game.TGameResult;

/**
 *
 * @author dat
 */
public class GameProcessor  implements Iface{

    @Override
    public boolean create(TGame value) throws TException {
        return GameBC.create(value);
    }

    @Override
    public boolean update(TGame value) throws TException {
        return GameBC.update(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return GameBC.remove(id);
    }

    @Override
    public boolean checkExists(String value) throws TException {
        return GameBC.checkExists(value);
    }

    @Override
    public TGameResult getById(long id) throws TException {
        return GameBC.getById(id);
    }

    @Override
    public TGameListResult getAllGame() throws TException {
        return GameBC.getAllGame();
    }

    @Override
    public boolean ping() throws TException {
        return true;
    }
    
}
