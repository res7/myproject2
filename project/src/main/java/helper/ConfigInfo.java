/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package helper;

import vn.outa.framework.util.ConfigUtil;
import vn.outa.framework.util.ConvertUtil;

/**
 *
 * @author dat
 */
public class ConfigInfo {
    public static final String REDIS_CACHE = "redis_cache";
    public static final String THRIFT_SERVICE = "thrift_service";
    public static final String THRIFT_CSM_SERVICE = "thrift_csm_module";

    /**
     * gearman.
     */
    public static final String GEARMAN_SERVER = "gearman_server";

//    public static final String FUNCTION_LOG = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_log"), "log");
    public static final String FUNCTION_USER = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_user"), "user");
    public static final String FUNCTION_AUTH = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_auth"), "auth");
    public static final String FUNCTION_GAMELOG = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_gamelog"), "gamelog");
    public static final String FUNCTION_GAME = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_game"), "game");

    /**
     * set config expire.
     */
    public static final int SET_EXPIRES_IN = 1800;

    /**
     * redis generate unique id.
     */
    public static final String AGENT_LOG_ID = "log_id";
}
