/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import cache.GameLogCA;
import db.model.GameLogDA;
import helper.ConfigInfo;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import project.thrift.auth.EJobType;
import project.thrift.gameLog.EStatusResult;
import project.thrift.gameLog.TGameLog;
import project.thrift.gameLog.TGameLogListResult;
import project.thrift.gameLog.TGameLogResult;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.UuidUtil;

/**
 *
 * @author dat
 */
public class GameLogBC {

    private static final Logger logger = Logger.getLogger(GameLogBC.class);

    public static boolean create(TGameLog gameLog) {
        try {
            gameLog.setId(getLastID() + 1);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(gameLog);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAMELOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            GameLogCA.setLastID(gameLog.getId());
            GameLogCA.setGameLog(gameLog, GameLogCA.isListKey(gameLog.getUserId()));

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TGameLog gameLog) {
        try {

            TGameLogResult valueUpdate = getById(gameLog.getId());
            if (valueUpdate.getStatus() == EStatusResult.FAIL) {
                return false;
            }

            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(gameLog);
            job.type = EJobType.UPDATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAMELOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            valueUpdate.gameLog.setGoldChange(gameLog.getGoldChange());
            valueUpdate.gameLog.setGameId(gameLog.getGameId());
            valueUpdate.gameLog.setCreateAt(gameLog.getCreateAt());
            valueUpdate.gameLog.setModifiedAt(gameLog.getModifiedAt());
            GameLogCA.setGameLog(valueUpdate.getGameLog(), GameLogCA.isListKey(gameLog.getId()));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean remove(long id) {
        try {
            if (id < 1) {
                return false;
            }
            TGameLogResult valueDelete = getById(id);
            if (valueDelete.getStatus() == EStatusResult.FAIL) {
                return false;
            }
            TGameLog value = new TGameLog();
            value.setId(id);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.DELETE;

            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAMELOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            GameLogCA.removeGameLog(id);
            GameLogCA.removeItemInList(valueDelete.getGameLog().getUserId(), id);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean updateStatus(TGameLog gameLog) {
        return true;
    }

    public static TGameLogResult getById(long id) {
        TGameLogResult result = null;
        try {
            TGameLog data = GameLogCA.getGameLog(id);
            if (data == null) {
                result = GameLogDA.getInstance().getById(id);
                GameLogCA.setGameLog(result.getGameLog(), false);
            } else {
                result = new TGameLogResult(EStatusResult.OK, data);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static TGameLogListResult getListByID(long userId, int offset, int count) {
        return GameLogDA.getInstance().getListByUser(userId);
    }

    public static TGameLogListResult getListRank(int offset, int count) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private static Long getLastID() {
         Long result = null;
        try{
            result = GameLogCA.getLastId();
            if(result > 0){
                return result;
            }
            
            result = GameLogDA.getInstance().getLastID();
            GameLogCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static TGameLogListResult getListByGameDay(long userId, long idGame, String modifiedAt, int offset, int count) {
            return GameLogDA.getInstance().getListByUserGameDay(userId, idGame, modifiedAt);

    }

}
