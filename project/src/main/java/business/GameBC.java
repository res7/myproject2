/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import cache.GameCA;
import db.model.GameDA;
import helper.ConfigInfo;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import project.thrift.auth.EJobType;
import project.thrift.game.EStatusResult;
import project.thrift.game.TGame;
import project.thrift.game.TGameListResult;
import project.thrift.game.TGameResult;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;

/**
 *
 * @author dat
 */
public class GameBC {

    private static final Logger logger = Logger.getLogger(GameBC.class);

    public static boolean create(TGame value) {
        try {
            if (checkExists(value.getGameName())) {
                return false;
            }
            value.setId(getLastID() + 1);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            GameCA.setLastID(value.getId());
            GameCA.setGame(value, GameCA.isListKey());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TGame value) {
        try {
            if (StringUtil.isNullOrEmpty(value.description) || StringUtil.isNullOrEmpty(value.gameName)) {
                return false;
            }
            TGameResult valueUpdate = getById(value.getId());
            if (valueUpdate.getStatus() == EStatusResult.FAIL) {
                return false;
            }

            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.UPDATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            valueUpdate.value.setGameName(value.getGameName());
            valueUpdate.value.setDescription(value.getDescription());
            GameCA.setGame(valueUpdate.getValue(), GameCA.isListKey());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean remove(long id) {
        try {
            if (id < 1) {
                return false;
            }
            TGame value = new TGame();
            value.setId(id);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.DELETE;

            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            GameCA.removeGame(id);
            GameCA.removeItemInList(id);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean checkExists(String value) {
        boolean result = GameDA.getInstance().checkExists(value);
        return result;
    }

    public static TGameResult getById(long id) {
        TGameResult result = null;
        try{
            TGame data = GameCA.getGame(id);
            if(data == null){
                result = GameDA.getInstance().getById(id);
                GameCA.setGame(result.getValue(), false);
            }else{
                result = new TGameResult(EStatusResult.OK, data);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;    }

    public static TGameListResult getAllGame() {
          TGameListResult result = new TGameListResult();
        
        try{
            List<TGame> list = new ArrayList<>();
            TGame data;
            List<Long> listId = GameCA.getList(0, -1);
            if(listId.isEmpty()){
                TGameListResult listData = GameDA.getInstance().getList("");
                for(TGame temp : listData.getListData()){
                    GameCA.addList(temp.getId(), temp.getId());
                }
                
                listId = GameCA.getList(0, -1);
            }
            
            for(Long id : listId){
                data = GameCA.getGame(id);
                if(data == null){
                    data = GameDA.getInstance().getById(id).getValue();
                    GameCA.setGame(data, true);
                }
                list.add(data);
            }

            result.setListData(list);
            result.setTotalRecord(list.size());
            result.setStatus(result.getTotalRecord() == 0 ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static Long getLastID() {
        Long result = null;
        try {
            result = GameCA.getLastId();
            if (result > 0) {
                return result;
            }

            result = GameDA.getInstance().getLastID();
            GameCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
}
