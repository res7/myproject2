/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import cache.UserCA;
import db.model.UserDA;
import helper.ConfigInfo;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import project.thrift.user.EJobType;
import project.thrift.user.EStatusResult;
import project.thrift.user.TRequestLogin;
import project.thrift.user.TUser;
import project.thrift.user.TUserListResult;
import project.thrift.user.TUserResult;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;

/**
 *
 *
 * @author dat
 */
public class UserBC {

    private static final Logger logger = Logger.getLogger(UserBC.class);

    public static boolean create(TUser value) {
        try {
            if (checkUserExists(value)) {
                return false;
            }
            value.setId(getLastID() + 1);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            System.out.println("4");

            UserCA.setLastID(value.getId());
            UserCA.setUser(value, UserCA.isListKey());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TUser user) {
        if (StringUtil.isNullOrEmpty(user.numberPhone) || StringUtil.isNullOrEmpty(user.email)
                || StringUtil.isNullOrEmpty(user.displayName)) {
            return false;
        }
        JobEnt job = new JobEnt();
        job.uuid = UuidUtil.makeString();
        job.data = JsonUtil.serialize(user);
        job.type = EJobType.UPDATE;
        if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
            return false;
        }
        TUserResult dataRedis = getById(user.getId());
//        dataRedis.value.setBirthDay(user.getBirthDay());
//        dataRedis.value.setGender(user.getGender());
//        dataRedis.value.setAddress(user.getAddress());
        dataRedis.value.setEmail(user.getEmail());
        UserCA.setUser(dataRedis.getValue(), false);
        return true;
    }

    public static TUserResult getById(long id) {
        TUserResult result = null;
        try {
            TUser data = UserCA.getUser(id);
            if (data == null) {
                result = UserDA.getInstance().getById(id);
                UserCA.setUser(result.getValue(), false);
            } else {
                result = new TUserResult(EStatusResult.OK, data);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static TUserListResult getAllUser() {
        TUserListResult result = null;
        try {
            List<Long> listID = UserCA.getList(0, -1);
            listID.forEach((id) -> result.addToListData(UserCA.getUser(id)));

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static Long getLastID() {
        Long result = null;
        try {
            result = UserCA.getLastId();
            if (result > 0) {
                return result;
            }
            result = UserDA.getInstance().getLastID();
            UserCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    public static boolean checkUserExists(TUser value) {
        boolean result = UserDA.getInstance().checkUserExists(value.getUserName(), value.getNumberPhone());
        return result;
    }

    public static boolean remove(long idUser) {
        try {
            if (idUser == 0) {
                return false;
            }
            TUser user = new TUser();
            user.setId(idUser);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(user);
            job.type = EJobType.DELETE;

            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            UserCA.removeUser(idUser);
            UserCA.removeItemInList(idUser);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean ban(long idUser) {
        TUser user = new TUser();
        user.setId(idUser);
        JobEnt job = new JobEnt();
        job.uuid = UuidUtil.makeString();
        job.data = JsonUtil.serialize(user);
        job.type = EJobType.BAN;
        if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
            return false;
        }
        TUserResult dataRedis = getById(idUser);
        dataRedis.value.setIsBan(true);
        UserCA.setUser(dataRedis.getValue(), false);
        return true;
    }

    public static boolean verify(long idUser) {
        TUser user = new TUser();
        user.setId(idUser);
        JobEnt job = new JobEnt();
        job.uuid = UuidUtil.makeString();
        job.data = JsonUtil.serialize(user);
        job.type = EJobType.VERIFY;
        if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
            return false;
        }
        TUserResult dataRedis = getById(idUser);
        dataRedis.value.setIsVerify(true);
        UserCA.setUser(dataRedis.getValue(), false);
        return true;

    }

//    public static boolean login(TRequestLogin login) {
////        throw new    UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
////        if (StringUtil.isNullOrEmpty(login.userName) || StringUtil.isNullOrEmpty(login.password)) {
////            return false;
////        }
////        JobEnt job = new JobEnt();
////        job.uuid = UuidUtil.makeString();
////        job.data = JsonUtil.serialize(login);
////        job.type = EJobType.LOGIN_USER;
////        if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
////            return false;
////        }
////        
//        return true;
//
//    }
    public static TUserResult login(TRequestLogin login) {
        
        try {
            TUserResult valueLogin = getByUserName(login.getUserName());
//            if (valueLogin.getStatus() == EStatusResult.FAIL) {
//                return valu
//            }
//            if (!valueLogin.value.getPassword().equals(encodePassword(password))) {
//                return 2;
//            }
                return valueLogin;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
        
    }

    public static TUserResult getByUserName(String userName) {
        TUserResult result = null;
        try {
            Long id = UserCA.getUserByUserName(userName);
            if (id == null) {
                TUserListResult listData = UserDA.getInstance().getList("");
                for (TUser temp : listData.getListData()) {
                    UserCA.addList(temp.getId(), temp.getUserName());
                }
                id = UserCA.getUserByUserName(userName);
                if (id == null) {
                    result = new TUserResult(EStatusResult.FAIL, null);
                    return result;
                }
            }
            result = getById(id);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

}
