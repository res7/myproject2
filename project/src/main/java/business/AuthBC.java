/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package business;

import static business.UserBC.getLastID;
import cache.AuthCA;
import db.model.AuthDA;
import helper.ConfigInfo;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import project.thrift.auth.EJobType;
import project.thrift.auth.EStatusResult;
import project.thrift.auth.TAuth;
import project.thrift.auth.TAuthResponse;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;

/**
 *
 * @author dat
 */
public class AuthBC {

    private static final Logger logger = Logger.getLogger(AuthBC.class);

    public static TAuthResponse getSecretKey(long UserID) {
        TAuthResponse tAuthRes = new TAuthResponse();
        TAuth result = null;
        try {
            TAuth data = AuthCA.getSecretKey(UserID);
            if (data == null) {
                tAuthRes = AuthDA.getInstance().getById(UserID);
//                tAuthRes.setAuth(result);
//                tAuthRes.setStatus(EStatusResult.OK);
                AuthCA.setAuth(result, false);
//                AuthCA.setSecretKey(result, false);
            } else {
                result = data;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return tAuthRes;
    }

    public static boolean create(TAuth auth) {
        try {
            if (isAuthExist(auth)) {
                return false;
            }
            auth.setId(getLastID() + 1);
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(auth);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_AUTH, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            AuthCA.setLastID(auth.getId());
            AuthCA.setAuth(auth, AuthCA.isListKey());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean remove(long UserID) {
        try {
            if (UserID == 0) {
                return false;
            }
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(UserID);
            job.type = EJobType.DELETE;

            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_AUTH, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            AuthCA.removeAuth(UserID);
            AuthCA.removeItemInList(UserID);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TAuth auth) {
        if (StringUtil.isNullOrEmpty(auth.secretkey)) {
            return false;
        }
        JobEnt job = new JobEnt();
        job.uuid = UuidUtil.makeString();
        job.data = JsonUtil.serialize(auth);
        job.type = EJobType.UPDATE;
        if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_AUTH, GearmanJob.JobPriority.NORMAL)) {
            return false;
        }
        TAuth dataRedis = getById(auth.getId()).getAuth();
        dataRedis.setSecretkey(auth.getSecretkey());
        AuthCA.setAuth(dataRedis, false);
        return true;
    }

    public static TAuthResponse getById(long id) {
        TAuthResponse result = null;
        try {
            TAuth data = AuthCA.getSecretKey(id);
            if (data == null) {
                result = AuthDA.getInstance().getById(id);
                AuthCA.setAuth(result.getAuth(), false);
            } else {
                result.setAuth(data);
                result.setStatus(EStatusResult.OK);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

    private static boolean isAuthExist(TAuth auth) {
        boolean result = AuthDA.getInstance().isAuthExist(auth.getUserId());
        return result;
    }

}
