/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cache;

import java.util.ArrayList;
import java.util.List;
import project.thrift.user.TUser;
import redis.clients.jedis.Jedis;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import zlib.db.RedisDBClient;

/**
 *
 * @author dat
 */
public class UserCA {

    public static final String USER_KEY = "db_game:user:%s";
    public static final String USER_LIST_KEY = "db_game:user_list";

    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(USER_KEY, "lastID");
            result = redis.set(key, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean isListKey() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(USER_LIST_KEY);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static long getLastId() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(USER_KEY, "lastID");
            String data = redis.get(key);
            if (data.isEmpty()) {
                return result;
            }

            result = Long.valueOf(data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean addList(double balance, String userName) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            redis.zadd(USER_LIST_KEY, balance, ConvertUtil.toString(userName));
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static TUser getUser(long id) {
        TUser result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(USER_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }

            result = JsonUtil.deserialize(data, TUser.class);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean setUser(TUser value, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {

            String key = String.format(USER_KEY, value.getId());

            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
            if (result && isAddList) {
                result = addList( value.getId(), value.getUserName());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean removeUser(long idUser) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(USER_KEY, idUser);
            result = redis.del(key) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean removeItemInList(long idUser) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        try {
            result = redis.zrem(USER_LIST_KEY, String.valueOf(idUser)) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static List<Long> getList(int recordStart, int pageSize) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(USER_LIST_KEY, recordStart, pageSize);
            for (String id : list) {
                result.add(Long.valueOf(id));
            }

        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
//    public static void ban(long idUser) {
//    }

    public static Long getUserByUserName(String userName) {
        Long result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zscore(USER_LIST_KEY, userName).longValue();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
}
