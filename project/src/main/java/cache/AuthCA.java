/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cache;

import project.thrift.auth.TAuth;
import redis.clients.jedis.Jedis;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import zlib.db.RedisDBClient;

/**
 *
 * @author dat
 */
public class AuthCA {

    public static final String AUTH_KEY = "db_game:auth:%s";
    public static final String AUTH_LIST_KEY = "db_game:auth_list";

    public static TAuth getSecretKey(long UserID) {
        TAuth result = null;
        Jedis redis = RedisDBClient.instance().getJedis();
        try {
            String key = String.format(AUTH_KEY, UserID);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }

            result = JsonUtil.deserialize(data, TAuth.class);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

//    public static boolean setSecretKey(TAuth auth, boolean b) {
//        boolean result = false;
//        Jedis redis = RedisDBClient.instance().getJedis();
//
//        try {
//
//            String key = String.format(AUTH_KEY, auth.getId());
//
//            result = redis.set(key, JsonUtil.serialize(auth)).equalsIgnoreCase("OK");
//            if (result && b) {
//                result = addList(auth.getId(), auth.getSecretkey());
//            }
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        } finally {
//            redis.close();
//        }
//        return result;
//    }

    public static boolean addList(long userId, String secretKey) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        try {
            redis.zadd(AUTH_LIST_KEY, userId, secretKey);
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static boolean isListKey() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(AUTH_LIST_KEY);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(AUTH_KEY, "lastID");
            result = redis.set(key, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static void setAuth(TAuth auth, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        try {

            String key = String.format(AUTH_KEY, auth.getId());

            result = redis.set(key, JsonUtil.serialize(auth)).equalsIgnoreCase("OK");
            if (result && isAddList) {
                result = addList(auth.getUserId(),auth.getSecretkey());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
    }

    public static boolean removeAuth(long UserID) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            String key = String.format(AUTH_KEY, UserID);
            result = redis.del(key) >= 0;
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;   
    }

    public static boolean removeItemInList(long UserID) {
         boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            result = redis.zrem(AUTH_LIST_KEY, String.valueOf(UserID)) >= 0;
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }

}
