/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cache;

import db.helper.ConfigInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import project.thrift.game.TGame;
import redis.clients.jedis.Jedis;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import zlib.db.RedisDBClient;

/**
 *
 * @author dat
 */
public class GameCA {

    public static final String GAME_KEY = "db_game:game:%s";
    public static final String GAME_LIST_KEY = "db_game:game_list";

    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_KEY, "lastID");
            result = redis.set(key, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean setGame(TGame value, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {

            String key = String.format(GAME_KEY, value.getId());

            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
            if (result && isAddList) {
                result = addList(value.getId(), value.getId());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean addList(double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            redis.zadd(GAME_LIST_KEY, score, ConvertUtil.toString(eventId));
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static boolean isListKey() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(GAME_LIST_KEY);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static long getLastId() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_KEY, "lastID");
            String data = redis.get(key);
            if (data.isEmpty()) {
                return result;
            }

            result = Long.valueOf(data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean removeGame(long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_KEY, eventId);
            result = redis.del(key) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean removeItemInList(long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zrem(GAME_LIST_KEY, String.valueOf(eventId)) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static TGame getGame(long id) {
        TGame result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }

            result = JsonUtil.deserialize(data, TGame.class);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static List<Long> getList(int recordStart, int pageSize) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(GAME_LIST_KEY, recordStart, pageSize);
            for (String id : list) {
                result.add(Long.valueOf(id));
            }

        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
}
