/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cache;

import java.util.ArrayList;
import java.util.List;
import project.thrift.gameLog.TGameLog;
import redis.clients.jedis.Jedis;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import zlib.db.RedisDBClient;

/**
 *
 * @author dat
 */
public class GameLogCA {

    public static final String GAME_LOG_KEY = "db_game:gold_log:%s";
    public static final String GAME_LOG_LIST_KEY = "db_game:gold_log_list:%s";
    public static final String GAME_LOG_LIST_DAY_KEY = "db_game:gold_log_list:%s_%s_%s";

    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_KEY, "lastID");
            result = redis.set(key, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean setGameLog(TGameLog value, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {

            String key = String.format(GAME_LOG_KEY, value.getId());

            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
            if (result && isAddList) {
                result = addListUser(value.getUserId(), value.getGoldChange(), value.getId());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean isListKey(long idUser) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_LIST_KEY, idUser);
            result = redis.exists(key);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static boolean addListUser(long idUser, double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_LIST_KEY, idUser);
            redis.zadd(key, score, ConvertUtil.toString(eventId));
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static boolean removeGameLog(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_KEY, id);
            result = redis.del(key) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static boolean removeItemInList(long userId, long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_LIST_KEY, userId);
            result = redis.zrem(key, String.valueOf(id)) >= 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static TGameLog getGameLog(long id) {
        TGameLog result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }

            result = JsonUtil.deserialize(data, TGameLog.class);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }

    public static List<Long> getListUser(long userId, int i, int i0) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_LIST_KEY, userId);
            List<String> list = redis.zrevrange(key, i, i0);
            for (String id : list) {
                result.add(Long.valueOf(id));
            }

        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static List<Long> getListUserGameDay(long userId, long idGame, String modifiedAt, int offset, int count) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_LIST_DAY_KEY, userId, idGame, modifiedAt);
            List<String> list = redis.zrevrange(key, offset, count);
            for (String id : list) {
                result.add(Long.valueOf(id));
            }

        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }

    public static Long getLastId() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(GAME_LOG_KEY, "lastID");
            String data = redis.get(key);
            if (data.isEmpty()) {
                return result;
            }

            result = Long.valueOf(data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
}
