/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daemon;

import helper.ConfigInfo;
import org.apache.log4j.Logger;
import project.thrift.game.GameService;
import project.thrift.gameLog.gameLogService;
import project.thrift.user.UserService;
import thrift.GameLogProcessor;
import thrift.GameProcessor;
import thrift.UserProcessor;
import vn.outa.framework.thrift.server.TServerFactory;
import vn.outa.framework.thrift.server.TServerManager;
import vn.outa.framework.thrift.server.TServerProcessor;
import vn.outa.framework.util.LogUtil;



/**
 *
 * @author dat
 */
public class RunService {
     private static final Logger logger = LogUtil.getLogger(RunService.class);

    public static void main(String[] args) throws Exception {

        TServerFactory tsf = new TServerFactory(ConfigInfo.THRIFT_SERVICE);
        tsf.addProcessor(new TServerProcessor(UserService.class, UserProcessor.class));
        tsf.addProcessor(new TServerProcessor(GameService.class, GameProcessor.class));
        tsf.addProcessor(new TServerProcessor(gameLogService.class, GameLogProcessor.class));

        TServerManager server = new TServerManager();
        server.add(tsf);
        server.start();
        logger.info("Thrift server start!");
        
    }
}
