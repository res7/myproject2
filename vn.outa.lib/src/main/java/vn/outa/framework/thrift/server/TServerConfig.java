package vn.outa.framework.thrift.server;

import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.thrift.helper.TConfigException;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.ConfigUtil;

public class TServerConfig {

    public String mode;
    public String host;
    public int port;
    public int maxThread;
    public int minThread;
    public String protocol;
    public boolean framed;

    public TServerConfig(final String section) {
        final String m = ConfigUtil.getString(section, "mode");
        if (StringUtil.isNullOrEmpty(m)) {
            throw new TConfigException("mode cannot be null or empty");
        }
        final String h = ConfigUtil.getString(section, "host");
        final Integer p = ConfigUtil.getInteger(section, "port");
        if (StringUtil.isNullOrEmpty(h) || p == null) {
            throw new TConfigException("host or port cannot be null or empty");
        }
        this.mode = m;
        this.host = h;
        this.port = p;
        this.protocol = ConvertUtil.toString((Object) ConfigUtil.getString(section, "protocol"), "binary");
        this.framed = ConvertUtil.toBoolean(ConfigUtil.getString(section, "framed"), true);
        this.maxThread = ConvertUtil.toInt((Object) ConfigUtil.getInteger(section, "maxthread"), 1024);
        this.minThread = ConvertUtil.toInt((Object) ConfigUtil.getInteger(section, "minthread"), 16);
    }
}
