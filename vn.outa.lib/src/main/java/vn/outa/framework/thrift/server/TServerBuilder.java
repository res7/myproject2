package vn.outa.framework.thrift.server;

public interface TServerBuilder
{
    TServerObject getServer();
}
