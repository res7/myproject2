package vn.outa.framework.thrift.client;

import vn.outa.framework.thrift.helper.TConfigException;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.ConfigUtil;

public class TClientConfig {

    public String host;
    public int port;
    public int timeout;
    public String protocol;
    public boolean framed;
    public boolean ping;
    public int maxActive;
    public int maxIdle;
    public int minIdle;
    public long maxWait;
    public int setMinEvictableIdleTimeMillis;
    public int setTimeBetweenEvictionRunsMillis;

    public TClientConfig(final String section) {
        final String h = ConfigUtil.getString(section, "host");
        final Integer p = ConfigUtil.getInteger(section, "port");
        if (StringUtil.isNullOrEmpty(h) || p == null) {
            throw new TConfigException("host or port cannot be null or empty");
        }
        this.host = h;
        this.port = p;
        this.timeout = ConvertUtil.toInt(ConfigUtil.getInteger(section, "timeout"));
        this.protocol = ConvertUtil.toString(ConfigUtil.getString(section, "protocol"), "binary");
        this.framed = ConvertUtil.toBoolean(ConfigUtil.getString(section, "framed"), true);
        this.ping = ConvertUtil.toBoolean(ConfigUtil.getString(section, "ping"), false);
        this.maxActive = ConvertUtil.toInt(ConfigUtil.getInteger(section, "maxactive"), 20);
        this.maxIdle = ConvertUtil.toInt(ConfigUtil.getInteger(section, "maxidle"), 8);
        this.minIdle = ConvertUtil.toInt(ConfigUtil.getInteger(section, "minidle"), 0);
        this.maxWait = ConvertUtil.toLong(ConfigUtil.getInteger(section, "maxwait"), -1L);
        this.setMinEvictableIdleTimeMillis = ConvertUtil.toInt(ConfigUtil.getInteger(section, "set_min_evictable_idle_time_millis"), 30000);
        this.setTimeBetweenEvictionRunsMillis = ConvertUtil.toInt(ConfigUtil.getInteger(section, "set_time_between_eviction_runs_millis"), 30000);
    }
}
