package vn.outa.framework.gearman;

import vn.outa.framework.util.UuidUtil;

public class JobEnts<T extends Enum>
{
    public String uuid;
    public T type;
    public String data;
    public long createdAt;
    
    public JobEnts() {
        this.uuid = UuidUtil.makeString();
        this.data = "";
        this.createdAt = System.currentTimeMillis();
    }
    
    public JobEnts(final String uuid, final T type, final String data, final long createdAt) {
        this.uuid = uuid;
        this.type = type;
        this.data = data;
        this.createdAt = createdAt;
    }
}
