package vn.outa.framework.db;

import java.sql.Connection;

/**
 *
 * @author admin
 */
public interface ConnectionIF {

    Connection borrow();

    void giveBack(final Connection p0);

    void invalidate(final Connection p0);
}
