package vn.outa.framework.db;

import vn.outa.framework.util.ConfigUtil;
import vn.outa.framework.util.ConvertUtil;


/**
 *
 * @author admin
 */
public class ConnectionConfig {

    public String driver;
    public String url;
    public String user;
    public String password;
    public int maxActive;
    public int maxIdle;
    public int minIdle;
    public long maxWait;
    public int setMinEvictableIdleTimeMillis;
    public int setTimeBetweenEvictionRunsMillis;

    public ConnectionConfig(final String section) {
        this.driver = ConvertUtil.toString(ConfigUtil.getString(section, "driver"));
        this.url = ConvertUtil.toString(ConfigUtil.getString(section, "url"));
        this.user = ConvertUtil.toString(ConfigUtil.getString(section, "user"));
        this.password = ConvertUtil.toString(ConfigUtil.getString(section, "password"));
        this.maxActive = ConvertUtil.toInt(ConfigUtil.getInteger(section, "maxactive"), 20);
        this.maxIdle = ConvertUtil.toInt(ConfigUtil.getInteger(section, "maxidle"), 8);
        this.minIdle = ConvertUtil.toInt(ConfigUtil.getInteger(section, "minidle"), 0);
        this.maxWait = ConvertUtil.toLong(ConfigUtil.getInteger(section, "maxwait"), -1L);
        this.setMinEvictableIdleTimeMillis = ConvertUtil.toInt(ConfigUtil.getInteger(section, "set_min_evictable_idle_time_millis"), 30000);
        this.setTimeBetweenEvictionRunsMillis = ConvertUtil.toInt(ConfigUtil.getInteger(section, "set_time_between_eviction_runs_millis"), 30000);
    }
}
